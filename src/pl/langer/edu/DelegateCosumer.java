package pl.langer.edu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Damian Langer on 31.05.16.
 */
public class DelegateCosumer {
    private List<TwoOperandDelegate> delegates;

    public DelegateCosumer() {
        delegates = new ArrayList<>();
    }

    public void addDelegate(TwoOperandDelegate delegate){
        delegates.add(delegate);
    }

    public void removeDelegate(TwoOperandDelegate delegate){
        delegates.remove(delegate);
    }

    public void apply(Long a, Long b){
        for (TwoOperandDelegate delegate : delegates){
            delegate.doit(a,b);
        }
    }
}
