package pl.langer.edu;

/**
 * Created by Damian Langer on 31.05.16.
 */
@FunctionalInterface
public interface TwoOperandDelegate {
    void doit(Long operandA, Long operandB);
}
