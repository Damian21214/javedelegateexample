package pl.langer.edu;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // Example 1
        System.out.println("====================== Example 1 ======================");
        DelegateCosumer delegateCosumer = new DelegateCosumer();
        TwoOperandDelegate addDelegate = (operandA, operandB) -> System.out.println(operandA + operandB);
        TwoOperandDelegate substractDelegate = (operandA, operandB) -> System.out.println(operandA-operandB);
        delegateCosumer.addDelegate(addDelegate);
        delegateCosumer.addDelegate(substractDelegate);
        delegateCosumer.addDelegate(Adder::add);

        delegateCosumer.apply(3L,3L);

        // Example 2
        System.out.println("====================== Example 2 ======================");
        List<String> stringList = Arrays.asList("a","b","c");

        stringList.stream().forEach(s -> System.out.println(s));

        // Example 3
        System.out.println("====================== Example 3 ======================");
        stringList.stream().forEach(System.out::println);

        // Example 4
        System.out.println("====================== Example 4 ======================");

        stringList.stream().forEach(s -> System.out.println(s.toUpperCase()));
    }
}
